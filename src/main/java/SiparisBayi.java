public class SiparisBayi {
    private Siparis siparis;
    private Bayi bayi;

    public SiparisBayi(Siparis siparis, Bayi bayi){
        this.siparis = siparis;
        this.bayi = bayi;
    }
    public Siparis getSiparis() {
        return siparis;
    }

    public void setSiparis(Siparis siparis) {
        this.siparis = siparis;
    }

    public Bayi getBayi() {
        return bayi;
    }

    public void setBayi(Bayi bayi) {
        this.bayi = bayi;
    }
}
