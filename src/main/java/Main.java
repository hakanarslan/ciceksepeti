import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {

        Bayi k = new Bayi("k", "41.049792", "29.003031");
        Bayi y = new Bayi("y", "41.069940", "29.019250");
        Bayi m = new Bayi("m", "41.049997", "29.026108");

        //csv den siparişleri alıp listeye atıyor
        List<Siparis> orderList = createSiparisList();
        //kuş uçuşu mesafeden optimal bayileri ve uzaklıklarını set ediyor
        getOptimalBayi(orderList, k, y, m);

        //hangi siparişin hangi bayiye atanacağını belirleyip döndürüyor
        List<SiparisBayi> siparisBayis = getSiparisBayi(orderList,k,m,y);

        //Siparisleri haritaya döküp tarayıcıda açıyor.
        System.out.println("Yandex Map 1 ile 100 arasına izin verdiği için, sipariş numarası haritadaki numaralara 99 eklenerek bulunuyor.");
        Map map = new Map();
        map.openMap(siparisBayis);
    }


    public static List<Siparis> createSiparisList() throws IOException {

        List<Siparis> siparisList = new ArrayList<Siparis>();
        String fileIn = "siparis.csv";

        String line = null;

        // Read all lines in from CSV file and add to studentList
        FileReader fileReader = new FileReader(fileIn);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        while ((line = bufferedReader.readLine()) != null) {
            String[] temp = line.split(",");
            int siparisID = Integer.parseInt(temp[0]);
            String latitude = temp[1];
            String longitude = temp[2];
            siparisList.add(new Siparis(siparisID, latitude, longitude));
        }
        bufferedReader.close();
        return siparisList;
    }


    public static void getOptimalBayi(List<Siparis> orderList, Bayi k, Bayi y, Bayi m) {
        double kLat,kLong,yLat,yLong,mLat,mLong;

        kLat=Double.parseDouble(k.getLatitude());
        kLong=Double.parseDouble(k.getLongitude());
        yLat=Double.parseDouble(y.getLatitude());
        yLong=Double.parseDouble(y.getLongitude());
        mLat=Double.parseDouble(m.getLatitude());
        mLong=Double.parseDouble(m.getLongitude());

        for (int i = 0; i < orderList.size(); i++) {
            Siparis myOrder=orderList.get(i);

            double orderLat = Double.parseDouble(myOrder.getLatitude());
            double orderLong =Double.parseDouble(myOrder.getLongitude());

            double kDiff=Math.sqrt(Math.pow((orderLat-kLat),2)+Math.pow(orderLong-kLong,2));
            double yDiff=Math.sqrt(Math.pow((orderLat-yLat),2)+Math.pow(orderLong-yLong,2));
            double mDiff=Math.sqrt(Math.pow((orderLat-mLat),2)+Math.pow(orderLong-mLong,2));

            String optimalBayiListesi;

            if(kDiff<=yDiff){
                if(yDiff<=mDiff)
                    optimalBayiListesi="kym";
                else if(kDiff<=mDiff)
                    optimalBayiListesi="kmy";
                else optimalBayiListesi="mky";
            }
            else if(yDiff<=mDiff){
                if(kDiff<=mDiff)
                optimalBayiListesi="ykm";
                else optimalBayiListesi="ymk";
            }
            else
                optimalBayiListesi="myk";

            myOrder.setkUzaklik(String.valueOf(kDiff));
            myOrder.setyUzaklik(String.valueOf(yDiff));
            myOrder.setmUzaklik(String.valueOf(mDiff));
            myOrder.setOptimalBayiListesi(optimalBayiListesi);
        }
    }
    public static List<SiparisBayi> getSiparisBayi(List<Siparis> siparisList,Bayi k,Bayi m,Bayi y){
        try {

            List<SiparisBayi> siparisBayiList = new ArrayList<>();
            int kMinCounter = 20;
            int yMinCounter = 35;
            int mMinCounter = 20;
            int kCounter = 10;
            int yCounter = 15;
            int mCounter = 60;

            siparisList.sort(new Comparator<Siparis>() {
                @Override
                public int compare(Siparis o1, Siparis o2) {
                    return o1.getkUzaklik().compareTo(o2.getkUzaklik());
                }
            });

            for (int i = 0; i < kMinCounter;i++){

                SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(0),k);
                siparisBayiList.add(siparisBayi);
                siparisList.remove(0);
            }

            siparisList.sort(new Comparator<Siparis>() {
                @Override
                public int compare(Siparis o1, Siparis o2) {
                    return o1.getmUzaklik().compareTo(o2.getmUzaklik());
                }
            });

            for (int i = 0; i < mMinCounter;i++){
                SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(0),m);
                siparisBayiList.add(siparisBayi);
                siparisList.remove(0);
            }

            siparisList.sort(new Comparator<Siparis>() {
                @Override
                public int compare(Siparis o1, Siparis o2) {
                    return o1.getyUzaklik().compareTo(o2.getyUzaklik());
                }
            });

            for (int i = 0; i < yMinCounter;i++){
                SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(0),y);
                siparisBayiList.add(siparisBayi);
                siparisList.remove(0);
            }

            for (int i = 0 ; i < siparisList.size();i++){

                if (siparisList.get(i).getOptimalBayiListesi().toCharArray()[0] == 'k'){

                    if (kCounter > 0){
                        SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(i),k);
                        siparisBayiList.add(siparisBayi);
                        kCounter--;
                    }
                    else{
                        if (siparisList.get(i).getOptimalBayiListesi().toCharArray()[1] == 'm'){
                            if (mCounter > 0){
                                SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(i),m);
                                siparisBayiList.add(siparisBayi);
                                mCounter--;
                            }
                            else{
                                if (yCounter > 0){
                                    SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(i),y);
                                    siparisBayiList.add(siparisBayi);
                                    yCounter--;
                                }
                            }
                        }
                        else{
                            if (yCounter > 0){
                                SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(i),y);
                                siparisBayiList.add(siparisBayi);
                                yCounter--;
                            }
                        }
                    }

                }

                else if (siparisList.get(i).getOptimalBayiListesi().toCharArray()[0] == 'm'){
                    if (mCounter > 0){
                        SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(i),m);
                        siparisBayiList.add(siparisBayi);
                        mCounter--;
                    }
                    else{
                        if (siparisList.get(i).getOptimalBayiListesi().toCharArray()[1] == 'k'){
                            if (kCounter > 0){
                                SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(i),k);
                                siparisBayiList.add(siparisBayi);
                                kCounter--;
                            }
                            else{
                                if (yCounter > 0){
                                    SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(i),y);
                                    siparisBayiList.add(siparisBayi);
                                    yCounter--;
                                }
                            }
                        }
                        else{
                            if (yCounter > 0){
                                SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(i),y);
                                siparisBayiList.add(siparisBayi);
                                yCounter--;
                            }
                        }
                    }
                }
                else{
                    if (yCounter > 0){
                        SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(i),y);
                        siparisBayiList.add(siparisBayi);
                        yCounter--;
                    }
                    else{
                        if (siparisList.get(i).getOptimalBayiListesi().toCharArray()[1] == 'k'){
                            if (kCounter > 0){
                                SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(i),k);
                                siparisBayiList.add(siparisBayi);
                                kCounter--;
                            }
                            else{
                                if (mCounter > 0){
                                    SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(i),m);
                                    siparisBayiList.add(siparisBayi);
                                    mCounter--;
                                }
                            }
                        }
                        else{
                            if (mCounter > 0){
                                SiparisBayi siparisBayi = new SiparisBayi(siparisList.get(i),m);
                                siparisBayiList.add(siparisBayi);
                                mCounter--;
                            }
                        }
                    }
                }
            }

            return siparisBayiList;
        }
        catch (Exception ex){
            return null;
        }
    }
}
