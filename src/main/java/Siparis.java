public class Siparis {
    private int siparisNo;
    private String latitude;
    private String longitude;
    private String optimalBayiListesi;
    //mky,ykm gib üç harften oluşacak
    //ilk aşamada con ile oluşturmayıp 2. aşamada set edilecek

    private String mUzaklik;
    private String kUzaklik;
    private String yUzaklik;

    public Siparis(){

    }

    public Siparis(int siparisNo, String latitude, String longitude) {
        this.siparisNo = siparisNo;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getSiparisNo() {
        return siparisNo;
    }

    public void setSiparisNo(int siparisNo) {
        this.siparisNo = siparisNo;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getOptimalBayiListesi() {
        return optimalBayiListesi;
    }

    public void setOptimalBayiListesi(String optimalBayiListesi) {
        this.optimalBayiListesi = optimalBayiListesi;
    }

    public String getmUzaklik() {
        return mUzaklik;
    }

    public void setmUzaklik(String mUzaklik) {
        this.mUzaklik = mUzaklik;
    }

    public String getkUzaklik() {
        return kUzaklik;
    }

    public void setkUzaklik(String kUzaklik) {
        this.kUzaklik = kUzaklik;
    }

    public String getyUzaklik() {
        return yUzaklik;
    }

    public void setyUzaklik(String yUzaklik) {
        this.yUzaklik = yUzaklik;
    }
}
