import java.awt.Desktop;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Map  {
    String url;
    private String baseUrl = "https://static-maps.yandex.ru/1.x/?lang=tr_TR&ll=29.020290000000,41.067730000000&size=650,450&z=13&l=map&pt=";
    StringJoiner joiner = new StringJoiner("~");

    public void openMap(List<SiparisBayi> siparisBayi){

        ArrayList<String> restOfUrl = new ArrayList<String>();

        for (int i = 0;i<siparisBayi.size();i++) {

            SiparisBayi sb = siparisBayi.get(i);

            int siparisNo = sb.getSiparis().getSiparisNo();
            String lat = sb.getSiparis().getLatitude();
            String lon = sb.getSiparis().getLongitude();
            String bayi = sb.getBayi().getName();
            int k = sb.getSiparis().getSiparisNo()-99;



            if (bayi.equals("k")) {

                restOfUrl.add(lon + "," + lat + ",pmrdl" + k );

            } else if (bayi.equals("m")) {

                restOfUrl.add(lon + "," + lat + ",pmbll"+ k );

            } else {

                restOfUrl.add(lon + "," + lat + ",pmgnl" + k);

            }
        }
        for(String s : restOfUrl){
            joiner.add(s);
        }
        url = baseUrl+joiner;
        try {
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                Desktop.getDesktop().browse(new URI(url));
            }
        }
        catch (Exception e){
            System.out.println("no");
        }
    }

}
